extends MeshInstance

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func grow_up():
	$Tween.interpolate_property(self, 'scale', Vector3(0,0,0), Vector3(1,1,1), 1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT, 0)
	$Tween.start()