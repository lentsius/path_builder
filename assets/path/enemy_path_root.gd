extends Spatial

const path_height = 2
var speed = 3

onready var path = $Path.curve

func _ready():
	print(to_global(path.get_point_position(0)))
	add_point(Vector3(0,0,0))

func _on_cubee_builder_block_built(pos):
	add_point(pos)

func add_point(pos):
	path.add_point(pos)
	print(path.get_baked_length())
	
func remove_last_point():
	var no = path.get_point_count()
	path.remove_point(no-1)
	
func _process(delta):
	var length = path.get_baked_length()
	if length != 0:
		$Path/OrientedPathFollow.unit_offset += speed/length * delta
	else:
		$Path/OrientedPathFollow.unit_offset += speed * delta

	if $Path/OrientedPathFollow.unit_offset >= 0.99:
		$Path/OrientedPathFollow.unit_offset = 0

func _on_cubee_builder_block_removed(pos):
	remove_last_point()
