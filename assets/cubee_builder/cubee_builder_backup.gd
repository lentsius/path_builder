extends Spatial

var can_x = true
var can_x_minus = true
var can_z = true
var can_z_minus = true

onready var x_ray = $x_plus
onready var x_minus_ray = $x_minus
onready var z_ray = $z_plus
onready var z_minus_ray = $z_minus

onready var cubee = preload('res://assets/cube/cubee.tscn')

const build_offset = 2

signal block_built(pos)

#arrays
var rays = []
var build_ability = []

# Called when the node enters the scene tree for the first time.
func _ready():
	rays.append(x_ray)
	rays.append(x_minus_ray)
	rays.append(z_ray)
	rays.append(z_minus_ray)
	
	build_ability.append(can_x)
	build_ability.append(can_x_minus)
	build_ability.append(can_z)
	build_ability.append(can_z_minus)
	
	check()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	check()
	if Input.is_action_just_pressed("ui_up"):
		build(0)
	if Input.is_action_just_pressed("ui_down"):
		build(1)
	if Input.is_action_just_pressed("ui_right"):
		build(2)
	if Input.is_action_just_pressed("ui_left"):
		build(3)

func check():
	for i in rays:
		if i.is_colliding():
			build_ability[rays.find(i)] = false
			$arrows.get_child(rays.find(i)).visible = false
		
		if !i.is_colliding():
			build_ability[rays.find(i)] = true
			$arrows.get_child(rays.find(i)).visible = true
			
	print(build_ability)
	
func build(no):
	if no == 0 and build_ability[no] == true:
		var new_cube = cubee.instance()
		var new_pos = self.global_transform.origin
		new_pos.x += build_offset
		new_cube.global_transform.origin = new_pos
		get_parent().add_child(new_cube)
		
		self.global_transform.origin = new_pos
		emit_signal("block_built", new_pos)
	else:
		pass
		# play a sound of not being able to build here
		
	if no == 1 and build_ability[no] == true:
		var new_cube = cubee.instance()
		var new_pos = self.global_transform.origin
		new_pos.x -= build_offset
		new_cube.global_transform.origin = new_pos
		get_parent().add_child(new_cube)
		
		self.global_transform.origin = new_pos
		emit_signal("block_built", new_pos)
	else:
		pass
		# play a sound of not being able to build here

	if no == 2 and build_ability[no] == true:
		var new_cube = cubee.instance()
		var new_pos = self.global_transform.origin
		new_pos.z += build_offset
		new_cube.global_transform.origin = new_pos
		get_parent().add_child(new_cube)
		
		self.global_transform.origin = new_pos
		emit_signal("block_built", new_pos)
	else:
		pass
		# play a sound of not being able to build here
		
	if no == 3 and build_ability[no] == true:
		var new_cube = cubee.instance()
		var new_pos = self.global_transform.origin
		new_pos.z -= build_offset
		new_cube.global_transform.origin = new_pos
		get_parent().add_child(new_cube)
		
		self.global_transform.origin = new_pos
		emit_signal("block_built", new_pos)
	else:
		pass
		# play a sound of not being able to build here
		
func build_cubee()