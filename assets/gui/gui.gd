extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	global.connect("length_changed", self, "_on_length_changed")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_length_changed():
	$length.text = str(global.build_length)
	var font = $length.get_font("font")
	$Tween.interpolate_property(font, 'size', 120, 60, 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()